package com.example.dao;

import java.util.List;

import com.example.model.*;

public interface EmployeeDao {
	
	 public List<Employee> getAllEmployees();
	 
	 public Employee findeEmployeeById(int id);
	 
	 public Object addEmployee(Employee employee);
	 
	 public void updateEmployee(Employee employee);
	 
	 public int deleteEmployee(int id);
	 
	 public List<Employee> getEmployeesByPage(int page_id, int total);
	 
}
