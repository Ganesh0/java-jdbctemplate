package com.example.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.common.Response;
import com.example.model.Employee;
import com.example.model.EmployeeRowMapper;





@Transactional
@Repository
public class EmployeeDaoImpl implements EmployeeDao {
	
	@Autowired
	 private JdbcTemplate jdbcTemplate;


	@Override
	public List<Employee> getAllEmployees() {
		// TODO Auto-generated method stub
		String query = "SELECT * from employees";
		  RowMapper<Employee> rowMapper = new EmployeeRowMapper();
		  List<Employee> list = jdbcTemplate.query(query, rowMapper);		  
		  return list;
	}

	@Override
	public Employee findeEmployeeById(int id) {
		// TODO Auto-generated method stub
		 String query = "SELECT * FROM employees WHERE employee_id = ?";
		  RowMapper<Employee> rowMapper = new BeanPropertyRowMapper<Employee>(Employee.class);
		  Employee employee = jdbcTemplate.queryForObject(query, rowMapper, id);
		  return employee;
	}

	@Override
	public Object addEmployee(Employee employee) {
		// TODO Auto-generated method stub
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String query = "INSERT INTO employees(first_name, last_name, email, phone, job_title, age) VALUES(?, ?, ?, ?, ?, ?)";
		  jdbcTemplate.update(connection -> {
              PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
              ps.setString(1, employee.getFirstName());
              ps.setString(2, employee.getLastName());
              ps.setString(3, employee.getEmail());
              ps.setString(4, employee.getPhone());
              ps.setString(5, employee.getJobTitle());
              ps.setInt(6, employee.getAge());
    		  return ps;
		  }, keyHolder);
		        Number key = keyHolder.getKey();			  
				Object result =  this.findeEmployeeById(key.intValue());
		        return result;
	}

	@Override
	public void updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		String query = "UPDATE employees SET first_name=?, last_name=?, email=?, phone=?, job_title=?, age=? WHERE employee_id=?";
		  jdbcTemplate.update(query, employee.getFirstName(), employee.getLastName(), employee.getEmail(), employee.getPhone(), employee.getJobTitle(), employee.getAge(), employee.getEmployeeId());
		
	}

	@Override
	public int deleteEmployee(int id) {
		// TODO Auto-generated method stub
		 int rowsAffected = 0;
		 String query = "DELETE FROM employees WHERE employee_id=?";
		 rowsAffected  = jdbcTemplate.update(query, id);	
		 return rowsAffected;
		
	}

	@Override
	public List<Employee> getEmployeesByPage(int page_id, int total) {
		// TODO Auto-generated method stub
		 String sql= "SELECT * FROM employees ORDER BY first_name ASC LIMIT "+(page_id-1)+","+total;
	        return jdbcTemplate.query(sql, new RowMapper<Employee>() {
	            public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
	            	Employee e = new Employee();  
		        	e.setEmployeeId(rs.getInt(1));
		        	e.setFirstName(rs.getString(2));
		        	e.setLastName(rs.getString(3));
	                e.setEmail(rs.getString(4));
	                e.setPhone(rs.getString(5));
	                e.setJobTitle(rs.getString(6));
	                e.setAge(rs.getInt(7));
	                Response res = new Response();
	                res.setPayload(e);
		            return e; 
	            }
	        });
	}

}
