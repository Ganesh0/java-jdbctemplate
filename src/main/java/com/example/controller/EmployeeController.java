package com.example.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.example.model.Employee;
import com.example.service.EmployeeServiceImpl;


import com.example.common.Response;


@Controller
@RequestMapping("/employee")
public class EmployeeController {
	
	 @Autowired
	 private EmployeeServiceImpl employeeService;
	 
	 @RequestMapping(value= {"/", "/list"}, method=RequestMethod.GET)
	 @ResponseBody
	 public Response getAllEmployees() {
	  Response res = new Response();
		  List<Employee> list = employeeService.getAllEmployees();
		  res.setSuccess(true);
		  res.setPayload(list);  
	      return res;
	 }
	 
	 @RequestMapping(value="/update/{id}", method=RequestMethod.GET)
	 @ResponseBody
	 public Response editEmployee(@PathVariable @Valid @NotNull int id) {
		 Response res = new Response();
		 try {
			 Employee employee = employeeService.findEmployeeById(id);
		     res.setSuccess(true);
		     res.setMessage("data successfully fetched");
		     res.setPayload(employee); 
		 } catch (EmptyResultDataAccessException e) {
			 res.setMessage(e.toString());
		 } 
	     return res;
	 }
	 
	 @RequestMapping(value="/save", method=RequestMethod.POST)
	 @ResponseBody
	 public Response saveOrUpdate(@RequestBody Employee employee) {
		 Response res = new Response();
	  if(employee.getEmployeeId() != null) {
	   if (employee.getAge() > 100) {
			  res.setMessage("age greater than 100 are not allowed to update data");
		  } else {
			   employeeService.updateEmployee(employee);
			  res.setSuccess(true);
			  res.setMessage("data successfully updated");
		  }
	  } else {
		  if (employee.getAge() > 100) {
			  res.setMessage("age greater than 100 are not allowed to create data");
		  } else {
			  Object result = employeeService.addEmployee(employee);
			  res.setSuccess(true);
			  res.setMessage("data successfully created");
			  res.setPayload(result);
		  }
		
	  }
	     return res;
	  
	 }
	 
	 @RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
	 @ResponseBody
	 public  Response deleteEmployee(@PathVariable("id") int id) {
		 Response res = new Response();
			 int result;
		     result = employeeService.deleteEmployee(id);
		     if (result == 1) {
			     res.setSuccess(true); 
			     res.setMessage("Data successfully deleted");;
		     } else {
		    	 res.setSuccess(false);
		    	 res.setMessage("No data exists for the specified id");;
		     }
	        return res;
	 }
	 
	 @RequestMapping(value="/list-page/{id}", method=RequestMethod.GET)
	 @ResponseBody
	 public Response paginate(@PathVariable("id") int page_id) {   
		 Response res = new Response();
	        int total = 5;
	        
	        try {
	        	 if(page_id == 1) {
	 	            // do nothing!
	 	        } else {            
	 	            page_id= (page_id-1)*total+1;  
	 	        }
	 	 
	 	        List<Employee> list = employeeService.getEmployeesByPage(page_id, total);
	 	        res.setSuccess(true);
	 	        
	 	        res.setPayload(list);
	 	 
	 	        return res; 
	        } catch (Exception e) {
	        	res.setMessage(e.toString());
	        	return res;
	        }
	        
	    }
	 	

}
