package com.example.common;

public class Response {
	
	private boolean success = false;
	private String message = "";
	private Object payload;
	
	public Response() {}
	
	public Response (boolean isSuccess) {
		this.setSuccess(isSuccess);
	}

	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Object getPayload() {
		return payload;
	}
	
	public void setPayload(Object payload) {
		this.payload = payload;
	}

}
