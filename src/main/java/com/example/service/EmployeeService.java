package com.example.service;

import java.util.List;

import com.example.model.Employee;

public interface EmployeeService {
	
	 public List<Employee> getAllEmployees();
	 
	 public Employee findEmployeeById(int id);
	 
	 public Object addEmployee(Employee employee);
	 
	 public void updateEmployee(Employee employee);
	 
	 public int deleteEmployee(int id);
	 
	 public List<Employee> getEmployeesByPage(int page_id, int total);
	 
}
