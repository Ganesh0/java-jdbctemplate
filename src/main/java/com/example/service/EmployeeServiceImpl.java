package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.EmployeeDaoImpl;
import com.example.model.Employee;


@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	 @Autowired
	 private EmployeeDaoImpl employeeDao;

	@Override
	public List<Employee> getAllEmployees() {
		// TODO Auto-generated method stub
		  return employeeDao.getAllEmployees();
	}

	@Override
	public Employee findEmployeeById(int id) {
		// TODO Auto-generated method stub
		  return employeeDao.findeEmployeeById(id);
	}

	@Override
	public Object addEmployee(Employee employee) {
		// TODO Auto-generated method stub
		  return employeeDao.addEmployee(employee);
	}

	@Override
	public void updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		  employeeDao.updateEmployee(employee);
	}

	@Override
	public int deleteEmployee(int id) {
		// TODO Auto-generated method stub
		  return employeeDao.deleteEmployee(id);
	}

	public List<Employee> getEmployeesByPage(int page_id, int total) {
		// TODO Auto-generated method stub
		return employeeDao.getEmployeesByPage(page_id, total);
	}

}
