# java-jdbctemplate

STEP 1===========> CREATE DATABASE IF NOT EXISTS jdbctutorial;

STEP 2===========> USE jdbctutorial;

STEP 3===========> CREATE TABLE IF NOT EXISTS employees (
  `employee_id` int(11) NOT NULL auto_increment,
  `first_name` varchar(45) NOT NULL default '',
  `last_name` varchar(45) NOT NULL default '',
  `email` varchar(45) NOT NULL default '',
  `phone` varchar(20) NOT NULL default '',
  `job_title` varchar(100) NOT NULL default '',
  `age` int(11) NOT NULL default '',
  PRIMARY KEY  (`employee_id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

